package com.example.loannt.spendingnote.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "NoteEntity")
data class NoteEntity(
    var date: Long,
    var note: String,
    var detail: String = "",
    var value: Float = 0.0f,
    var categoryId: Long? = null
) : Serializable{
    @PrimaryKey(autoGenerate = true)
    var noteID: Long? = null
}
