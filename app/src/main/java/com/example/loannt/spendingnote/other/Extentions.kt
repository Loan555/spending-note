package com.example.loannt.spendingnote.other

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.example.loannt.spendingnote.data.model.CategoryNote
import java.lang.Exception
import java.math.BigInteger
import java.security.MessageDigest
import java.text.SimpleDateFormat
import java.util.*

/**
 * Dialog extensions
 */
fun Context.showDialogAlert(
    title: String,
    mess: String,
    callBack: () -> Unit
) {
    val builder: AlertDialog.Builder = AlertDialog.Builder(this)
    builder.setTitle(title)
    builder.setMessage(mess)
    builder.setPositiveButton("OK") { _, _ ->
        callBack()
    }
    builder.setNegativeButton("Cancel") { dialog, _ -> dialog.dismiss() }
    builder.show()
}

fun Context.showDialogInputText(
    title: String,
    callBack: (String) -> Unit,
    default: String = "Name"
) {
    val builder: AlertDialog.Builder = AlertDialog.Builder(this)
    builder.setTitle(title)
    val input = EditText(this)
    input.hint = "Enter Text"
    input.inputType = InputType.TYPE_CLASS_TEXT
    input.setText(default)
    builder.setView(input)
    builder.setPositiveButton("OK") { _, _ ->
        callBack(input.text.toString())
    }
    builder.setNegativeButton("Cancel") { dialog, _ -> dialog.dismiss() }
    builder.show()
}

fun Context.showDateDialog(title: String, callBack: (String) -> Unit) {
    DatePickerDialog(this).apply {
        setOnDateSetListener { _, y, m, d ->
            val mm = (m + 1).toString().padStart(2, '0')
            callBack("$y/$mm/$d")
        }
        setTitle(title)
        show()
    }
}

fun Context.showDateDialogReturnCalendar(title: String, callBack: (Long) -> Unit) {
    DatePickerDialog(this).apply {
        setOnDateSetListener { _, y, m, d ->
            val date = Calendar.getInstance().apply {
                set(Calendar.HOUR_OF_DAY, 0)
                set(Calendar.YEAR, y)
                set(Calendar.MONTH, m)
                set(Calendar.DAY_OF_MONTH, d)
            }
            callBack(date.timeInMillis)
        }
        setTitle(title)
        show()
    }
}

fun Context.showDialogRadioButton(
    title: String = "",
    callBack: (Int) -> Unit,
    default: Int = 0,
    options: Array<String> = arrayOf()
) {
    var itemClick = default
    AlertDialog.Builder(this).apply {
        setTitle(title)
        setSingleChoiceItems(
            options, default
        ) { _, p ->
            itemClick = p
        }
        setPositiveButton("OK") { dialog, p ->
            dialog.dismiss()
            callBack(itemClick)
        }
        setNegativeButton("Cancel") { dialog, _ ->
            dialog.dismiss()
        }
        show()
    }
}

/**
 *  date time format extensions
 */
@SuppressLint("SimpleDateFormat")
fun String.strToDate(): Long? = try {
    SimpleDateFormat("$DATE_FORMAT HH:mm:ss").parse("$this 12:00:00")?.time
} catch (e: Exception) {
    Log.e(TAG, "Date format error!")
    null
}

@SuppressLint("SimpleDateFormat")
fun Long.dateToString(str: String = DATE_FORMAT) = try {
    SimpleDateFormat(str).format(
        this
    )
} catch (e: Exception) {
    Log.e(TAG, "Date format error!")
    null
}


/** access share preferences */
fun Context.putPreferences(key: String, data: String) {
    val pref = getSharedPreferences(KEY_PREF, AppCompatActivity.MODE_PRIVATE)
    pref.edit().putString(key, data).apply()
}

fun Context.getPreferences(key: String): String? {
    val pref = getSharedPreferences(KEY_PREF, AppCompatActivity.MODE_PRIVATE)
    return pref.getString(key, null)
}

/** hash password for security */
fun String.md5(): String {
    val md = MessageDigest.getInstance("MD5")
    return BigInteger(1, md.digest(this.toByteArray())).toString(16).padStart(32, '0')
}