package com.example.loannt.spendingnote.data.db

import android.util.Log
import androidx.lifecycle.LiveData
import com.example.loannt.spendingnote.data.model.CategoryNote
import com.example.loannt.spendingnote.data.model.CategoryWithNotes
import com.example.loannt.spendingnote.data.model.NoteEntity
import com.example.loannt.spendingnote.other.TAG
import com.example.loannt.spendingnote.other.dateToString

class NoteRepository(private val dao: NoteDao) {
    /** Notes */
    suspend fun insert(v: NoteEntity) = dao.insert(v)

    suspend fun update(v: NoteEntity) = dao.update(v)

    suspend fun delete(v: NoteEntity) = dao.delete(v)

    suspend fun clearData() = dao.clearData()

    fun getAllNotes() = dao.getAllWithLiveData()

    fun getNotesBetweenTime(start: Long, end: Long) = dao.getNotesBetweenTime(start, end)

    fun getCategoryWithNotesBetweenTime(start: Long, end: Long) =
        dao.getCategoryWithNotesBetweenTime(start, end)

    /** Category */
    fun getAllCategory() = dao.getAllCategoryWithLiveData()
    fun getAllCategory2() = dao.getAllCategory()

    suspend fun insertCategory(e: CategoryNote) = dao.insertCategory(e)

    suspend fun updateCategory(e: CategoryNote) = dao.update(e)

    suspend fun deleteCategory(id: Long) {
        dao.deleteNoteByCategoryID(id)
        dao.deleteCategory(id)
    }

    /** all */
    fun getCategoryWithNotes(type: String) = dao.getCategoryWithNotes(type)

    fun findCategoryWithNotesByTime(
        type: String,
        start: Long,
        end: Long
    ): LiveData<MutableList<CategoryWithNotes>> {
        Log.d(TAG, "start time = ${start.dateToString()}   end = ${end.dateToString()}")
        return dao.findCategoryWithNotesByTime(type, start, end)
    }
}