package com.example.loannt.spendingnote.ui.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.loannt.spendingnote.ui.adapter.IconColorAdapter
import com.example.loannt.spendingnote.data.model.CategoryNote
import com.example.loannt.spendingnote.databinding.ActivityAddCategoryBinding
import com.example.loannt.spendingnote.other.*
import com.example.loannt.spendingnote.ui.base.BaseActivity

class AddCategoryActivity : BaseActivity() {
    private lateinit var binding: ActivityAddCategoryBinding
    private var data = CategoryNote(
        "",
        EnumColor.values()[0].name,
        EnumIcon.values()[0].name,
        EnumTypeNote.Expense.name
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddCategoryBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val dataIntent =
            intent.getBundleExtra(KEY_CATEGORY)?.getSerializable(KEY_CATEGORY) as? CategoryNote
        if (dataIntent != null) {
            data = dataIntent
            binding.btnEdit.visibility = View.VISIBLE
        }

        intent.getStringExtra(KEY_TYPE_CATEGORY)?.let {
            data.typeNote = it
            if (it == EnumTypeNote.Expense.name) {
                binding.radioExpense.isChecked = true
            } else {
                binding.radioIncome.isChecked = true
            }
            Log.d(TAG, "data = $it")
        }

        binding.rcvColor.layoutManager =
            GridLayoutManager(this, 2, LinearLayoutManager.HORIZONTAL, false)
        binding.rcvIcon.layoutManager =
            GridLayoutManager(this, 2, LinearLayoutManager.HORIZONTAL, false)
        binding.rcvIcon.adapter = IconColorAdapter(false, callBack)
        binding.rcvColor.adapter = IconColorAdapter(true, callBack)
        binding.btnAdd.setOnClickListener {
            getInput()?.let {
                val bundle = Bundle()
                bundle.putSerializable(KEY_ADD_CATEGORY, it)
                val intent = Intent().putExtra(KEY_ADD_CATEGORY, bundle)
                setResult(RESULT_OK, intent)
                finish()
            }
        }
        binding.btnEdit.setOnClickListener {
            getInput()?.let {
                val bundle = Bundle()
                bundle.putSerializable(KEY_ADD_CATEGORY, it)
                val intent = Intent().putExtra(KEY_ADD_CATEGORY, bundle)
                setResult(RESULT_OK, intent)
                finish()
            }
        }
        binding.groupRadioBtn.setOnCheckedChangeListener { _, i ->
            data.typeNote = when (i) {
                binding.radioIncome.id -> {
                    EnumTypeNote.Income.name
                }
                else -> {
                    EnumTypeNote.Expense.name
                }
            }
        }
    }

    private val callBack: (color: EnumColor?, icon: EnumIcon?) -> Unit = { c, i ->
        if (c != null) {
            data.color = c.name
        }
        if (i != null) {
            data.ic = i.name
        }
    }

    private fun getInput(): CategoryNote? {
        val title = binding.edtTitle.text.toString()
        if (title.isNotEmpty()) {
            return data.copy(nameCategory = title)
        } else {
            Toast.makeText(this, "Title is must not empty!", Toast.LENGTH_SHORT).show()
        }
        return null
    }
}