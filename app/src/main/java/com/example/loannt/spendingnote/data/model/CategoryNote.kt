package com.example.loannt.spendingnote.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "Category")
data class CategoryNote(
    var nameCategory: String,
    var color: String,
    var ic: String,
    var typeNote: String
) : Serializable {
    @PrimaryKey(autoGenerate = true)
    var categoryId: Long? = null
}
