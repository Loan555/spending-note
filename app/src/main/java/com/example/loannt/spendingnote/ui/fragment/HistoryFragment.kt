package com.example.loannt.spendingnote.ui.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.loannt.spendingnote.data.model.CategoryNote
import com.example.loannt.spendingnote.data.model.NoteEntity
import com.example.loannt.spendingnote.databinding.FragmentHistoryBinding
import com.example.loannt.spendingnote.other.TAG
import com.example.loannt.spendingnote.other.dateToString
import com.example.loannt.spendingnote.ui.adapter.NoteDayAdapter
import com.example.loannt.spendingnote.ui.custom.MonthYearPickerDialog
import com.example.loannt.spendingnote.vm.MainViewModel
import com.example.loannt.spendingnote.vm.MyViewModelFactory
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.TimeUnit


class HistoryFragment : Fragment() {
    private val vm: MainViewModel by activityViewModels {
        MyViewModelFactory(requireActivity().application)
    }
    private lateinit var binding: FragmentHistoryBinding
    private lateinit var adapter: NoteDayAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = NoteDayAdapter(
            onClickCallBack = itemCallBack,
            leftCallback = { vm.deleteNote(it) },
            rightCallback = itemCallBack
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHistoryBinding.inflate(layoutInflater, container, false)
        onDateChange()  //init tvDate to filer list
        initListener()  //init onclick form view
        initRcv()
        initSearch()
        return binding.root
    }

    private fun initRcv() {
        binding.rcvListNote.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.rcvListNote.adapter = adapter

        //change list when category change
        vm.allCategory.observe(viewLifecycleOwner) { list ->
            val listCate = mutableMapOf<Long, CategoryNote>()
            list.forEach { category ->
                category.categoryId?.let {
                    listCate[it] = category
                }
            }
            adapter.categoryChange(listCate)
        }
    }

    private fun initListener() {
        binding.btnClickBack.setOnClickListener {
            vm.date.add(Calendar.MONTH, -1)
            onDateChange()
        }
        binding.btnClickNext.setOnClickListener {
            vm.date.add(Calendar.MONTH, 1)
            onDateChange()
        }
        binding.tcMonthYear.setOnClickListener {
            val pd = MonthYearPickerDialog(callBackGetTime, vm.date)
            pd.show(parentFragmentManager, "MonthYearPickerDialog")
        }
    }

    @SuppressLint("CheckResult")
    private fun initSearch() {
        Observable.create<String> {
            binding.searchViewHistory.setOnQueryTextListener(object :
                SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    it.onComplete()
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    if (!it.isDisposed) {
                        it.onNext(newText!!)
                    }
                    return false
                }

            })
        }.debounce(500L, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                adapter.search(it)
                Log.d(TAG, it.toString())
            }, {
                Toast.makeText(requireContext(), "ERROR", Toast.LENGTH_LONG).show()
            }, {
                Toast.makeText(requireContext(), "COMPLETE", Toast.LENGTH_LONG).show()
            })
    }

    private fun setView(listNote: MutableList<NoteEntity>) {
        val valueList = listNote.map {
            it.value
        }
        binding.sumExpense.text = valueList.filter { it < 0 }.sum().toString()
        binding.sumIncome.text = valueList.filter { it >= 0 }.sum().toString()
        binding.sum.text = valueList.sum().toString()
        adapter.listChange(listNote)
    }

    /**
     * callback
     */
    private val itemCallBack: (note: NoteEntity) -> Unit = {
        val action = HistoryFragmentDirections.actionHistoryFragmentToNoteFragment(noteData = it)
        findNavController().navigate(action)
    }

    private val callBackGetTime: (Int, Int) -> Unit = { year, month ->
        vm.date.set(year, month - 1, 15)
        onDateChange()
    }

    private fun onDateChange() {
        binding.tcMonthYear.text = "${vm.date.timeInMillis.dateToString("yyyy.MMMM")}"
        vm.findNotesMonth(vm.date).observe(viewLifecycleOwner) {
            setView(it)
        }
    }
}