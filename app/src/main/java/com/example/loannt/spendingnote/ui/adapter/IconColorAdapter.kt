package com.example.loannt.spendingnote.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.loannt.spendingnote.other.EnumColor
import com.example.loannt.spendingnote.other.EnumIcon
import com.example.loannt.spendingnote.R
import com.example.loannt.spendingnote.databinding.AdapterItemColorIcBinding

class IconColorAdapter(
    private val isColorAdapter: Boolean = true,
    private val callBack: (eColor: EnumColor?, eIcon: EnumIcon?) -> Unit,
) :
    RecyclerView.Adapter<IconColorAdapter.ViewHolder>() {
    private val listColor = EnumColor.values()
    private val listIcon = EnumIcon.values()
    private var context: Context? = null
    private var clickPosition: Int = 0

    inner class ViewHolder(private val binding: AdapterItemColorIcBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("NotifyDataSetChanged")
        fun onBindColor(e: EnumColor) {
            context?.run {
                binding.itemLayout.setBackgroundColor(getColor(e.colorID))
            }
            binding.itemLayout.setOnClickListener {
                clickPosition = layoutPosition
                callBack(e, null)
                notifyDataSetChanged()
            }
            if (clickPosition == layoutPosition) {
                binding.itemLayout.setStrokeColorResource(R.color.bg_btn)
            } else {
                binding.itemLayout.setStrokeColorResource(R.color.white)
            }
        }

        @SuppressLint("NotifyDataSetChanged")
        fun onBindIcon(e: EnumIcon) {
            context?.run {
                binding.itemLayout.setImageResource(e.iconID)
            }
            binding.itemLayout.setOnClickListener {
                clickPosition = layoutPosition
                callBack(null, e)
                notifyDataSetChanged()
            }
            if (clickPosition == layoutPosition) {
                binding.itemLayout.setStrokeColorResource(R.color.bg_btn)
            } else {
                binding.itemLayout.setStrokeColorResource(R.color.white)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            AdapterItemColorIcBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        context = parent.context
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (isColorAdapter) holder.onBindColor(listColor[position])
        else holder.onBindIcon(listIcon[position])
    }

    override fun getItemCount(): Int = if (isColorAdapter) listColor.size
    else listIcon.size

}