package com.example.loannt.spendingnote.ui.fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.*
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.loannt.spendingnote.*
import com.example.loannt.spendingnote.ui.activity.AddCategoryActivity
import com.example.loannt.spendingnote.ui.adapter.CategoryAdapter
import com.example.loannt.spendingnote.data.model.CategoryNote
import com.example.loannt.spendingnote.data.model.NoteEntity
import com.example.loannt.spendingnote.databinding.FragmentNoteBinding
import com.example.loannt.spendingnote.other.*
import com.example.loannt.spendingnote.vm.AddNoteViewModel
import com.example.loannt.spendingnote.vm.MainViewModel
import com.example.loannt.spendingnote.vm.MyViewModelFactory
import kotlin.math.abs

class NoteFragment : Fragment() {
    private lateinit var binding: FragmentNoteBinding
    private val addViewModel: AddNoteViewModel by viewModels()
    private val vm: MainViewModel by activityViewModels {
        MyViewModelFactory(requireActivity().application)
    }
    private var adapter: CategoryAdapter? = null
    private val args: NoteFragmentArgs by navArgs()

    private fun openActivityForResult(data: CategoryNote?) {
        val intentResult = Intent(requireContext(), AddCategoryActivity::class.java)
        if (data != null) {
            val bundle = Bundle()
            bundle.putSerializable(KEY_CATEGORY, data)
            intentResult.putExtra(KEY_CATEGORY, bundle)
        }
        intentResult.putExtra(KEY_TYPE_CATEGORY, addViewModel.typeNote.name)
        resultLauncher.launch(intentResult)
    }

    private val resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data
                data?.getBundleExtra(KEY_ADD_CATEGORY)?.let {
                    val category = it.getSerializable(KEY_ADD_CATEGORY) as? CategoryNote
                    if (category != null) {
                        if (category.categoryId != null) {
                            vm.updateCategory(category)
                        } else vm.insertCategory(category)
                    }
                }
            }
        }

    @SuppressLint("SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreateView onCreate")
        args.noteData?.let {
            addViewModel.noteAdd.postValue(it)
        }
        adapter = CategoryAdapter(
            callBack = callBack
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Log.d(TAG, "onCreateView NoteFragment")
        binding = FragmentNoteBinding.inflate(layoutInflater)
        binding.vm = addViewModel
        binding.lifecycleOwner = viewLifecycleOwner
        initView()
        initListener()
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    private fun initView() {
        addViewModel.noteAdd.observe(viewLifecycleOwner) {
            if (it.noteID != null && binding.btnUpdate.visibility != View.VISIBLE) {
                binding.btnUpdate.visibility = View.VISIBLE
            }
            //update view when it change
            if (addViewModel.typeNote == EnumTypeNote.Expense) {
                binding.groupRadioBtn.check(binding.radioExpense.id)
            } else binding.groupRadioBtn.check(binding.radioIncome.id)
            binding.tvDate.text = it.date.dateToString()
            binding.edtTitle.setText(it.note)
            binding.edtDetail.setText(it.detail)
            binding.edtValue.setText(abs(it.value).toString())
            val type = vm.allCategory.value?.find { cate ->
                it.categoryId == cate.categoryId
            }
            //update list category to chose when item change
            if (type?.typeNote != null) {
                updateListCategory(EnumTypeNote.valueOf(type.typeNote), it.categoryId)
            }
        }

        //init list category
        binding.rcvCategory.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.rcvCategory.adapter = adapter

        adapter?.swipeCallback(swipeLeftCallBack, swipeRightCallBack, requireContext())?.let {
            ItemTouchHelper(it).attachToRecyclerView(binding.rcvCategory)
        }

        //init radio button
        binding.groupRadioBtn.setOnCheckedChangeListener { _, i ->
            when (i) {
                binding.radioIncome.id -> {
                    addViewModel.typeNote = EnumTypeNote.Income
                    binding.edtValue.setTextColor(Color.parseColor("#FF018786"))
                }
                else -> {
                    addViewModel.typeNote = EnumTypeNote.Expense
                    binding.edtValue.setTextColor(Color.parseColor("#EE5007"))
                }
            }
            adapter?.filterTypeCategory(addViewModel.typeNote)
            binding.btnAdd.text = "${getString(R.string.add)} ${addViewModel.typeNote}"
        }

        //observe live data to update view
        vm.allCategory.observe(viewLifecycleOwner) {
            adapter?.listChangeWithFilter(it, addViewModel.typeNote)
        }
    }

    private fun updateListCategory(type: EnumTypeNote, idClick: Long?) {
        if (type == EnumTypeNote.Expense) {
            binding.groupRadioBtn.check(binding.radioExpense.id)
        } else binding.groupRadioBtn.check(binding.radioIncome.id)
        adapter?.setItemCheck(idClick)
    }

    private fun initListener() {
        binding.btnAdd.setOnClickListener {
            getInput()?.let {
                Toast.makeText(requireContext(), "$it Saved!", Toast.LENGTH_SHORT).show()
                vm.insertNote(it)
                clearText()
            }
        }
        binding.tvDate.setOnClickListener {
            requireContext().showDateDialog("Date") {
                it.strToDate()?.let { newDate ->
                    binding.tvDate.text = it
                    addViewModel.noteAdd.value?.date = newDate
                }
            }
        }
        binding.btnUpdate.setOnClickListener {
            getInput()?.let { it1 ->
                it1.noteID = addViewModel.noteAdd.value?.noteID
                vm.update(it1)
                Toast.makeText(requireContext(), "Data updated!", Toast.LENGTH_SHORT).show()
                requireActivity().onBackPressed()
            }
            Log.d(TAG, "update ${getInput()}")
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun getInput(): NoteEntity? {
        val title = binding.edtTitle.text.toString()
        val detail = binding.edtDetail.text.toString()
        val typeNote = addViewModel.typeNote
        var value = binding.edtValue.text.toString().toFloatOrNull()
        return if (title.isNotEmpty() && value != null && value > 0 && addViewModel.noteAdd.value?.categoryId != null) {
            if (typeNote == EnumTypeNote.Expense) {
                value *= -1
            }
            addViewModel.noteAdd.value?.copy(note = title, detail = detail, value = value)
        } else {
            var messError = ""
            when {
                title.isEmpty() -> {
                    messError = "Note is empty!"
                }
                (value == null || value <= 0) -> {
                    messError = "Value is invalidate!"
                }
                (addViewModel.noteAdd.value?.categoryId == null) -> {
                    messError = "You not chose category!"
                }
            }
            Toast.makeText(requireContext(), messError, Toast.LENGTH_SHORT)
                .show()
            null
        }
    }

    private fun clearText() {
        binding.edtValue.setText("0.0")
        binding.edtDetail.setText("")
        binding.edtTitle.setText("")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy add note fragment")
    }

    /**
     * Call back from recycler view category note
     */
    private val callBack: (CategoryNote) -> Unit = { c ->
        if (c.categoryId == null) {
            openActivityForResult(null)
        } else {
            addViewModel.noteAdd.value?.categoryId = c.categoryId!!
        }
    }

    private val swipeLeftCallBack: (CategoryNote) -> Unit = { c ->
        if (c.categoryId != null) {
            requireContext().showDialogAlert(
                "Delete category",
                "Are you sure delete this item? All notes related to this item will be deleted!"
            ) {
                vm.deleteCategory(c.categoryId!!)
            }
        }
    }

    private val swipeRightCallBack: (CategoryNote) -> Unit = { c ->
        if (c.categoryId != null) {
            openActivityForResult(c)
        }
    }

}