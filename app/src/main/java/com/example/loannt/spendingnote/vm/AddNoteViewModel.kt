package com.example.loannt.spendingnote.vm

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.loannt.spendingnote.other.EnumTypeNote
import com.example.loannt.spendingnote.data.model.NoteEntity
import java.util.*

/**
 * ViewModel to check add data
 */
class AddNoteViewModel : ViewModel() {
    val noteAdd: MutableLiveData<NoteEntity> = MutableLiveData(
        NoteEntity(
            date = Calendar.getInstance().timeInMillis,
            note = "",
            detail = "",
            value = 0.0f,
            categoryId = null
        )
    )
    var typeNote = EnumTypeNote.Expense
}