package com.example.loannt.spendingnote.data.model

import androidx.room.Embedded
import androidx.room.Relation

data class CategoryWithNotes(
    @Embedded val category: CategoryNote,
    @Relation(
        parentColumn = "categoryId",
        entityColumn = "categoryId"
    )
    val listNote: MutableList<NoteEntity>
)
