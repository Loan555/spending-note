package com.example.loannt.spendingnote.ui.fragment

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.loannt.spendingnote.R
import com.example.loannt.spendingnote.data.model.CategoryWithNotes
import com.example.loannt.spendingnote.data.model.NoteEntity
import com.example.loannt.spendingnote.databinding.FragmentHomeBinding
import com.example.loannt.spendingnote.other.*
import com.example.loannt.spendingnote.ui.custom.MonthYearPickerDialog
import com.example.loannt.spendingnote.vm.MainViewModel
import com.example.loannt.spendingnote.vm.MyViewModelFactory
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import java.util.*
import kotlin.math.abs


class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding
    private val vm: MainViewModel by activityViewModels {
        MyViewModelFactory(requireActivity().application)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(layoutInflater, container, false)
        initPieChart(binding.pieChartExpense)
        initPieChart(binding.pieChartIncome)

        //init radio button
        if (vm.typeNote == EnumTypeNote.Expense) {
            binding.groupRadioBtn.check(binding.radioExpense.id)
        } else {
            binding.groupRadioBtn.check(binding.radioIncome.id)
        }
        expenseChecked(vm.typeNote == EnumTypeNote.Expense)
        binding.groupRadioBtn.setOnCheckedChangeListener { _, i ->
            when (i) {
                binding.radioIncome.id -> {
                    vm.typeNote = EnumTypeNote.Income
                    expenseChecked(false)
                }
                else -> {
                    vm.typeNote = EnumTypeNote.Expense
                    expenseChecked(true)
                }
            }
        }

        //init view
        checkFilter()

        binding.itemFilterClick.setOnClickListener {
            val popupMenu = PopupMenu(requireContext(), it)
            popupMenu.menuInflater.inflate(R.menu.popup_menu, popupMenu.menu)
            popupMenu.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.monthYear -> {
                        val pd = MonthYearPickerDialog(callBackGetMonthYear, vm.date)
                        pd.show(parentFragmentManager, "MonthYearPickerDialog")
                    }
                    R.id.year -> {
                        val pd = MonthYearPickerDialog(callBackGetYear, vm.date)
                        pd.show(parentFragmentManager, "MonthYearPickerDialog")
                    }
                    R.id.date -> {
                        requireContext().showDateDialogReturnCalendar("Chose start time") { startTime ->
                            requireContext().showDateDialogReturnCalendar("Chose end time") { endTime ->
                                callBackGetStartToEnd(startTime, endTime)
                            }
                        }
                    }
                }
                true
            }
            popupMenu.show()
        }

        //init livedata observe
        vm.allCategoryExpense.observe(viewLifecycleOwner) { listCateWithNote ->
            setDataToPieChart(binding.pieChartExpense, listCateWithNote)
        }
        vm.allCategoryIncome.observe(viewLifecycleOwner) {
            setDataToPieChart(binding.pieChartIncome, it)
        }
        return binding.root
    }

    private fun expenseChecked(bool: Boolean) {
        if (bool) {
            binding.pieChartExpense.visibility = View.VISIBLE
            binding.pieChartIncome.visibility = View.GONE
        } else {
            binding.pieChartExpense.visibility = View.GONE
            binding.pieChartIncome.visibility = View.VISIBLE
        }
    }

    private fun checkFilter() {
        binding.tvTypeFilter.text = vm.typeHomeFilter.name
        when (vm.typeHomeFilter) {
            EnumArraySpinner.Month -> {
                callBackGetMonthYear(
                    vm.dateStartFilter.get(Calendar.YEAR),
                    vm.dateStartFilter.get(Calendar.MONTH)
                )
            }
            EnumArraySpinner.Year -> {
                callBackGetYear(
                    vm.dateStartFilter.get(Calendar.YEAR),
                    vm.dateStartFilter.get(Calendar.MONTH)
                )
            }
            EnumArraySpinner.Date -> {
                callBackGetStartToEnd(
                    vm.dateStartFilter.timeInMillis,
                    vm.dateEndFilter.timeInMillis
                )
            }
        }
    }

    private val callBackGetStartToEnd: (Long, Long) -> Unit = { startTime, endTime ->
        vm.typeHomeFilter = EnumArraySpinner.Date
        var text = "From ${startTime.dateToString()}"
        text += "\nTo ${endTime.dateToString()}"
        binding.tvTimeFilter.text = text
        vm.callDataStartToEnd(startTime, endTime)
        binding.tvTypeFilter.text = vm.typeHomeFilter.name
        vm.findNotesDate(vm.dateStartFilter, vm.dateEndFilter).observe(viewLifecycleOwner) {
//            initPieChart(it)
        }
    }

    private val callBackGetMonthYear: (Int, Int) -> Unit = { year, month ->
        vm.typeHomeFilter = EnumArraySpinner.Month
        binding.tvTypeFilter.text = vm.typeHomeFilter.name
        val time = Calendar.getInstance().apply { set(year, month - 1, 1) }
        val strTime = time.timeInMillis.dateToString("yyyy.MM")
        binding.tvTimeFilter.text = strTime
        vm.callDataByMonth(time.timeInMillis)
        vm.findNotesMonthHome(vm.dateStartFilter).observe(viewLifecycleOwner) {
//            initPieChart(it)
        }
    }

    @SuppressLint("SetTextI18n")
    private val callBackGetYear: (Int, Int) -> Unit = { year, _ ->
        vm.typeHomeFilter = EnumArraySpinner.Year
        binding.tvTimeFilter.text = "$year.All"
        val time = Calendar.getInstance().apply { set(Calendar.YEAR, year) }
        vm.callDataByYear(time.timeInMillis)
        binding.tvTypeFilter.text = vm.typeHomeFilter.name
        vm.findNotesYear(vm.dateStartFilter).observe(viewLifecycleOwner) {
//            initPieChart(it)
        }
    }

    private fun initPieChart(list: MutableList<CategoryWithNotes>) {
        val listExpense = list.filter { it.category.typeNote == EnumTypeNote.Expense.name }
        val listIncome = list.filter { it.category.typeNote == EnumTypeNote.Income.name }
        Log.e(TAG, "listExpense size ${listExpense.size} = $listExpense")
        Log.e(TAG, "listIncome size ${listIncome.size} = $listIncome")

        setDataToPieChart(binding.pieChartExpense, listExpense)
        setDataToPieChart(binding.pieChartIncome, listIncome)
    }

    private fun initPieChart(pieChart: PieChart) {
        pieChart.description.text = ""
        pieChart.setUsePercentValues(true)
        pieChart.legend.orientation = Legend.LegendOrientation.HORIZONTAL
        pieChart.legend.isWordWrapEnabled = true
    }

    private fun setDataToPieChart(pieChart: PieChart, list: List<CategoryWithNotes>) {
        pieChart.setUsePercentValues(true)
        val dataEntries = ArrayList<PieEntry>()
        val colors: ArrayList<Int> = ArrayList()
        list.forEach {
            val value = abs(it.listNote.map { note ->
                note.value
            }.sum())
            if (value != 0.0F) {
                dataEntries.add(PieEntry(value, it.category.nameCategory))
                colors.add(requireContext().getColor(EnumColor.valueOf(it.category.color).colorID))
            }
        }
        val dataSet = PieDataSet(dataEntries, "")
        val data = PieData(dataSet)

        // In Percentage
        data.setValueFormatter(PercentFormatter())
        dataSet.sliceSpace = 0.5f
        dataSet.colors = colors
        pieChart.data = data
        data.setValueTextSize(12f)
        pieChart.animateY(1000, Easing.EaseInOutQuad)

        //create hole in center
        pieChart.isDrawHoleEnabled = false
        pieChart.setHoleColor(Color.WHITE)
        pieChart.invalidate()
        if (pieChart.id == binding.pieChartExpense.id) {
            binding.tvExpense.text = dataEntries.map { it.value }.sum().toString()
            binding.tvSumBudget.text =
                (binding.tvIncome.text.toString().toFloat() - binding.tvExpense.text.toString()
                    .toFloat()).toString()
        } else {
            binding.tvIncome.text = dataEntries.map { it.value }.sum().toString()
            binding.tvSumBudget.text =
                (binding.tvIncome.text.toString().toFloat() - binding.tvExpense.text.toString()
                    .toFloat()).toString()
        }
        pieChart.invalidate()
    }
}