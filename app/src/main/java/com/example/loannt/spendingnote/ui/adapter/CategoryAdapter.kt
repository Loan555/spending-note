package com.example.loannt.spendingnote.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.loannt.spendingnote.other.EnumColor
import com.example.loannt.spendingnote.other.EnumIcon
import com.example.loannt.spendingnote.other.EnumTypeNote
import com.example.loannt.spendingnote.R
import com.example.loannt.spendingnote.ui.custom.SwipeItemTouchHelper
import com.example.loannt.spendingnote.data.model.CategoryNote
import com.example.loannt.spendingnote.databinding.AdapterCategoryBinding

class CategoryAdapter(
    private val list: MutableList<CategoryNote> = mutableListOf(),
    private val callBack: (e: CategoryNote) -> Unit,
    private var clickID: Long? = -1L
) :
    RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    private val listFilter: MutableList<CategoryNote> = mutableListOf()
    private lateinit var context: Context

    inner class CategoryViewHolder(private val binding: AdapterCategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("NotifyDataSetChanged")
        fun onBind(e: CategoryNote) {
            binding.imgIcon.setImageResource(EnumIcon.valueOf(e.ic).iconID)
            binding.tvTitle.text = e.nameCategory
            context.run {
                binding.tvTitle.setTextColor(getColor(EnumColor.valueOf(e.color).colorID))
                binding.imgIcon.setColorFilter(getColor(EnumColor.valueOf(e.color).colorID))
            }
            binding.itemLayout.setOnClickListener {
                clickID = e.categoryId
                callBack(e)
                notifyDataSetChanged()
            }
            if (clickID == e.categoryId) {
                binding.itemLayout.setBackgroundResource(R.drawable.bg_edt)
            } else {
                binding.itemLayout.setBackgroundResource(R.drawable.bg_btn_shape)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val binding =
            AdapterCategoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        context = parent.context
        return CategoryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.onBind(listFilter[position])
    }

    override fun getItemCount(): Int = listFilter.size

    fun listChangeWithFilter(newList: MutableList<CategoryNote>, type: EnumTypeNote?) {
        list.clear()
        list.addAll(newList)
        val editItem = CategoryNote(
            "Edit", EnumColor.Color1.name,
            EnumIcon.Add.name, EnumTypeNote.Other.name
        ).apply {
            categoryId = null
        }
        list.add(editItem)
        filterTypeCategory(type)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun filterTypeCategory(type: EnumTypeNote?) {
        when (type) {
            EnumTypeNote.Expense -> {
                listFilter.clear()
                listFilter.addAll(list.filter {
                    it.typeNote == EnumTypeNote.Expense.name || it.typeNote == EnumTypeNote.Other.name
                })
            }
            EnumTypeNote.Income -> {
                listFilter.clear()
                listFilter.addAll(list.filter {
                    it.typeNote == EnumTypeNote.Income.name || it.typeNote == EnumTypeNote.Other.name
                })
            }
            else -> return
        }
        notifyDataSetChanged()
    }

    fun setItemCheck(id: Long?) {
        clickID = id
        notifyDataSetChanged()
    }

    fun swipeCallback(
        left: (e: CategoryNote) -> Unit,
        right: (e: CategoryNote) -> Unit,
        ct: Context
    ) = SwipeItemTouchHelper(
        ct,
        swipeLeftCallback = { position ->
            notifyItemChanged(position)
            left(listFilter[position])
        },
        swipeRightCallback = { position ->
            notifyItemChanged(position)
            right(listFilter[position])
        })
}