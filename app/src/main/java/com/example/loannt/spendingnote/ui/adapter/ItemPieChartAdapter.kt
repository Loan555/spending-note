//package com.example.loannt.spendingnote.ui.adapter
//
//import android.annotation.SuppressLint
//import android.view.LayoutInflater
//import android.view.ViewGroup
//import androidx.recyclerview.widget.RecyclerView
//import com.example.loannt.spendingnote.databinding.AdapterPieChartBinding
//import org.eazegraph.lib.models.PieModel
//
//class ItemPieChartAdapter(
//    private val list: MutableList<PieModel>
//) :
//    RecyclerView.Adapter<ItemPieChartAdapter.ViewHolder>() {
//
//    inner class ViewHolder(private val binding: AdapterPieChartBinding) :
//        RecyclerView.ViewHolder(binding.root) {
//        @SuppressLint("SetTextI18n")
//        fun onBind(item: PieModel) {
//            binding.viewColorPie.setBackgroundColor(item.color)
//            binding.tvItemPie.text = item.legendLabel + " ${item.value}"
//        }
//    }
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        return ViewHolder(
//            AdapterPieChartBinding.inflate(
//                LayoutInflater.from(parent.context),
//                parent,
//                false
//            )
//        )
//    }
//
//    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        holder.onBind(list[position])
//    }
//
//    override fun getItemCount(): Int = list.size
//
//    /** set list data to rcv item */
//    @SuppressLint("NotifyDataSetChanged")
//    fun setList(newList: MutableList<PieModel>) {
//        list.clear()
//        list.addAll(newList)
//        notifyDataSetChanged()
//    }
//}