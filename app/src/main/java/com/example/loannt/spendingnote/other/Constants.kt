package com.example.loannt.spendingnote.other

import com.example.loannt.spendingnote.R
import kotlin.text.Typography.dollar

const val TAG = "TAG_DEBUG"
const val KEY_DATA = "KEY_DATA"
const val KEY_ADD_CATEGORY = "KEY_ADD_CATEGORY"
const val KEY_UPDATE_CATEGORY = "KEY_UPDATE_CATEGORY"
const val KEY_TYPE_CATEGORY = "KEY_TYPE_CATEGORY"
const val KEY_PREF = "KEY_PREF"
const val DATE_FORMAT = "yyyy/MM/dd"
const val KEY_CATEGORY = "KEY_ID_CATEGORY"
const val KEY_CURRENCY = "KEY_CURRENCY"

//enum data
enum class EnumCategoryNote(val ic: String, val nameCategory: String, val color: String) {
    NEC(EnumIcon.NEC.name, "Necessity account", EnumColor.Color1.name),
    LTSA(EnumIcon.LTSA.name, "Long-term saving for spending account", EnumColor.Color2.name),
    EDUC(EnumIcon.EDUC.name, "Education", EnumColor.Color3.name),
    Play(EnumIcon.Play.name, "Play", EnumColor.Color4.name),
    FFA(EnumIcon.FFA.name, "Financial freedom account", EnumColor.Color5.name),
    Give(EnumIcon.Give.name, "Give", EnumColor.Color6.name),
    Other(EnumIcon.Other.name, "Other", EnumColor.Color7.name)
}

enum class EnumTypeNote {
    Expense,
    Income,
    Other
}

enum class EnumColor(val colorID: Int) {
    Color1(R.color.item1),
    Color2(R.color.item2),
    Color3(R.color.item3),
    Color4(R.color.item4),
    Color5(R.color.item5),
    Color6(R.color.item6),
    Color7(R.color.item7),
    Color8(R.color.item8),
    Color9(R.color.item9),
    Color10(R.color.item10),
    Color11(R.color.item11)
}

enum class EnumIcon(val iconID: Int) {
    NEC(R.drawable.ic_restaurant),
    LTSA(R.drawable.ic_savings),
    EDUC(R.drawable.ic_education),
    Play(R.drawable.ic_shoping),
    FFA(R.drawable.ic_ffa),
    Give(R.drawable.ic_share),
    Other(R.drawable.ic_money),
    Add(R.drawable.ic_baseline_add),
    Salary(R.drawable.ic_money),
}

enum class EnumArraySpinner {
    Month, Year, Date
}

enum class EnumCurrency(titleID: Int) {
    Dollar(R.string.dollar), VND(R.string.vnd)
}