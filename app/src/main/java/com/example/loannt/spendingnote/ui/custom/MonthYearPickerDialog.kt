package com.example.loannt.spendingnote.ui.custom

import android.R
import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.example.loannt.spendingnote.databinding.DatePickerDialogBinding
import java.util.*

class MonthYearPickerDialog(
    private val callBack: (Int, Int) -> Unit,
    time: Calendar = Calendar.getInstance()
) : DialogFragment() {
    private val cal = time.clone() as Calendar

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder: AlertDialog.Builder = AlertDialog.Builder(activity)
        val binding = DatePickerDialogBinding.inflate(layoutInflater)

        binding.pickerMonth.minValue = 1
        binding.pickerMonth.maxValue = 12
        binding.pickerMonth.value = cal.get(Calendar.MONTH) + 1

        val year: Int = cal.get(Calendar.YEAR)
        binding.pickerYear.minValue = year - 100
        binding.pickerYear.maxValue = year + 100
        binding.pickerYear.value = year

        builder.setView(binding.root) // Add action buttons
            .setPositiveButton(R.string.ok) { _, _ ->
                callBack(
                    binding.pickerYear.value,
                    binding.pickerMonth.value
                )
            }
            .setNegativeButton(R.string.cancel) { _, _ ->
                dismiss()
            }
        return builder.create()
    }
}