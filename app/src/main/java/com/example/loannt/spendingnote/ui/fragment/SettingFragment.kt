package com.example.loannt.spendingnote.ui.fragment

import android.accounts.AccountManager.KEY_PASSWORD
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.loannt.spendingnote.ui.activity.BlockActivity
import com.example.loannt.spendingnote.databinding.FragmentSettingBinding
import com.example.loannt.spendingnote.other.*

class SettingFragment : Fragment() {
    private lateinit var binding: FragmentSettingBinding
    private var defaultCurrency: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentSettingBinding.inflate(layoutInflater, container, false)
        requireContext().getPreferences(KEY_CURRENCY)?.let {
            defaultCurrency = EnumCurrency.valueOf(it).ordinal
            binding.tvCurrency.text = it
        }
        binding.itemSettingPass.setOnClickListener {
            val intent = Intent(requireContext(), BlockActivity::class.java)
            intent.putExtra(KEY_DATA, true)
            startActivity(intent)
        }
        binding.itemSettingCurrency.setOnClickListener {
            requireContext().showDialogRadioButton(
                "Setting currency unit",
                callBack = {
                    val currency = EnumCurrency.values()[it].name
                    requireContext().putPreferences(KEY_CURRENCY, currency)
                    binding.tvCurrency.text = currency
                },
                default = defaultCurrency,
                EnumCurrency.values().map {
                    it.name
                }.toTypedArray()
            )
        }
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        if (requireContext().getPreferences(KEY_PASSWORD) != null) {
            binding.tvEnable.text = "Enable"
            binding.tvEnable.setTextColor(Color.GREEN)
        } else {
            binding.tvEnable.setTextColor(Color.GRAY)
            binding.tvEnable.text = "Disable"
        }
    }
}