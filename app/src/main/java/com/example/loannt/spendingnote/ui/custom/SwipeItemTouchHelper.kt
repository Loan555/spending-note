package com.example.loannt.spendingnote.ui.custom

import android.content.Context
import android.graphics.*
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.graphics.drawable.toBitmap
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.loannt.spendingnote.R

class SwipeItemTouchHelper(
    private val context: Context,
    private val swipeLeftCallback: (Int) -> Unit,
    private val swipeRightCallback: (Int) -> Unit
) : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
    private val p = Paint()
    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return true
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val position = viewHolder.adapterPosition
        if (direction == ItemTouchHelper.LEFT) {
            swipeLeftCallback(position)
        } else if (direction == ItemTouchHelper.RIGHT) {
            swipeRightCallback(position)
        }
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            val itemView = viewHolder.itemView
            val height = itemView.bottom.toFloat() - itemView.top.toFloat()
            val width = height / 3
            if (dX < 0) {
                p.color = Color.RED
                val background = RectF(
                    itemView.right.toFloat() + dX,
                    itemView.top.toFloat(),
                    itemView.right.toFloat(),
                    itemView.bottom.toFloat()
                )
                c.drawRect(background, p)
                val icon = AppCompatResources.getDrawable(context, R.drawable.ic_delete)?.toBitmap()
                val margin = (dX / 5 - width) / 2
                val iconDest = RectF(
                    itemView.right.toFloat() + margin,
                    itemView.top.toFloat() + width,
                    itemView.right.toFloat() + (margin + width),
                    itemView.bottom.toFloat() - width
                )
                if (icon != null) {
                    c.drawBitmap(icon, null, iconDest, p)
                }
            }
            if (dX > 0) {
                p.color = Color.BLUE
                val background = RectF(
                    itemView.left.toFloat(),
                    itemView.top.toFloat(),
                    itemView.left.toFloat() + dX,
                    itemView.bottom.toFloat()
                )
                c.drawRect(background, p)
                val icon = AppCompatResources.getDrawable(context, R.drawable.ic_edit)?.toBitmap()
                val margin = (dX / 5 - width) / 2
                val iconDest = RectF(
                    margin,
                    itemView.top.toFloat() + width,
                    margin + width,
                    itemView.bottom.toFloat() - width
                )
                if (icon != null) {
                    c.drawBitmap(icon, null, iconDest, p)
                }
            }
        } else {
            c.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR)
        }
        super.onChildDraw(
            c,
            recyclerView,
            viewHolder,
            dX / 5,
            dY,
            actionState,
            isCurrentlyActive
        )
    }
}