package com.example.loannt.spendingnote.ui.activity

import android.accounts.AccountManager.KEY_PASSWORD
import android.content.Intent
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.example.loannt.spendingnote.R
import com.example.loannt.spendingnote.databinding.ActivityMainBinding
import com.example.loannt.spendingnote.other.getPreferences
import com.example.loannt.spendingnote.ui.base.BaseActivity

class MainActivity : BaseActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    private lateinit var appBarConfig: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        //check setting password
        getPreferences(KEY_PASSWORD)?.let {
            startActivity(Intent(this, BlockActivity::class.java))
        }
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //init navigation with navigation controller
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.fragmentViewMain) as NavHostFragment
        navController = navHostFragment.findNavController()

        //create appbar to remove back button on search fragment
        appBarConfig = AppBarConfiguration(
            setOf(
                R.id.homeFragment,
                R.id.historyFragment,
                R.id.settingFragment
            )
        )
        binding.bottomNav.setupWithNavController(navController)
    }
}