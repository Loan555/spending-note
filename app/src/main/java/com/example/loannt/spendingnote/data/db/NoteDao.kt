package com.example.loannt.spendingnote.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.loannt.spendingnote.data.model.CategoryNote
import com.example.loannt.spendingnote.data.model.CategoryWithNotes
import com.example.loannt.spendingnote.data.model.NoteEntity

@Dao
interface NoteDao {
    /**
     * work with noteEntity
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(e: NoteEntity)

    @Delete
    suspend fun delete(e: NoteEntity)

    @Update
    suspend fun update(e: NoteEntity)

    @Query("DELETE FROM `NoteEntity`")
    suspend fun clearData()

    //NOTE: This query will run on background thread by room, don't need to create thread
    @Query("SELECT * FROM `NoteEntity` ORDER BY date DESC LIMIT 1000")
    fun getAllWithLiveData(): LiveData<MutableList<NoteEntity>>

    @Query("SELECT * FROM `NoteEntity` WHERE date >= :startTime AND date < :endTime ORDER BY date DESC LIMIT 500")
    fun getNotesBetweenTime(startTime: Long, endTime: Long): LiveData<MutableList<NoteEntity>>

    @Transaction
    @Query("SELECT * FROM Category JOIN NoteEntity ON Category.categoryId = NoteEntity.categoryId WHERE date >= :startTime AND date < :endTime ORDER BY date DESC LIMIT 500")
    fun getCategoryWithNotesBetweenTime(
        startTime: Long,
        endTime: Long
    ): LiveData<MutableList<CategoryWithNotes>>

    /**
     * work with categoryEntity
     */
    @Query("SELECT * FROM Category")
    fun getAllCategoryWithLiveData(): LiveData<MutableList<CategoryNote>>

    @Query("SELECT * FROM Category")
    fun getAllCategory(): List<CategoryNote>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCategory(e: CategoryNote)

    @Update
    suspend fun update(c: CategoryNote)

    @Query("DELETE FROM Category WHERE categoryId = :id ")
    suspend fun deleteCategory(id: Long)

    @Query("DELETE FROM NoteEntity WHERE categoryId = :id ")
    suspend fun deleteNoteByCategoryID(id: Long)

    /**
     * work with category with notes
     */
    @Transaction
    @Query("SELECT * FROM Category WHERE typeNote = :type")
    fun getCategoryWithNotes(type: String): LiveData<MutableList<CategoryWithNotes>>

    @Transaction
    @Query(
        "SELECT * FROM NoteEntity " +
                "INNER JOIN CATEGORY ON NoteEntity.categoryId = Category.categoryId " +
                "WHERE Category.typeNote = :type " +
                "AND NoteEntity.date BETWEEN :startTime " +
                "AND :endTime"
    )
    fun findCategoryWithNotesByTime(
        type: String,
        startTime: Long,
        endTime: Long
    ): LiveData<MutableList<CategoryWithNotes>>
}