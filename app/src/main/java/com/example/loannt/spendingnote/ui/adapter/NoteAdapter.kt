package com.example.loannt.spendingnote.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.loannt.spendingnote.other.EnumColor
import com.example.loannt.spendingnote.other.EnumIcon
import com.example.loannt.spendingnote.data.model.CategoryNote
import com.example.loannt.spendingnote.data.model.NoteEntity
import com.example.loannt.spendingnote.databinding.AdapterNoteBinding
import com.example.loannt.spendingnote.ui.custom.SwipeItemTouchHelper

class NoteAdapter(
    private val list: MutableList<NoteEntity> = mutableListOf(),
    private val listCategory: MutableMap<Long, CategoryNote> = mutableMapOf(),
    private val itemCallBack: (note: NoteEntity) -> Unit
) :
    RecyclerView.Adapter<NoteAdapter.NoteViewHolder>() {
    private var context: Context? = null

    inner class NoteViewHolder(private val binding: AdapterNoteBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun onBind(note: NoteEntity) {
            val categoryNote = listCategory[note.categoryId]
            binding.tvTitle.text = categoryNote?.nameCategory
            if (note.value > 0) {
                binding.tvValue.text = "+${note.value}"
            } else {
                binding.tvValue.text = note.value.toString()
            }
            binding.tvDetail.text = " (${note.note})"
            if (categoryNote != null && context != null) {
                context?.getColor(EnumColor.valueOf(categoryNote.color).colorID)?.let {
                    binding.tvTitle.setTextColor(it)
                    binding.tvDetail.setTextColor(it)
                    binding.imgIcon.setColorFilter(it)
                }
                binding.imgIcon.setImageResource(EnumIcon.valueOf(categoryNote.ic).iconID)
            }
            binding.itemLayout.setOnClickListener {
                itemCallBack(note)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        val binding = AdapterNoteBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        context = parent.context
        return NoteViewHolder(binding)
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        holder.onBind(list[position])
    }

    override fun getItemCount(): Int = list.size

    fun swipeCallback(
        left: (e: NoteEntity) -> Unit,
        right: (e: NoteEntity) -> Unit,
        ct: Context
    ) = SwipeItemTouchHelper(
        ct,
        swipeLeftCallback = { position ->
            notifyItemChanged(position)
            left(list[position])
        },
        swipeRightCallback = { position ->
            notifyItemChanged(position)
            right(list[position])
        })
}