package com.example.loannt.spendingnote.ui.activity

import android.accounts.AccountManager.KEY_PASSWORD
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.loannt.spendingnote.*
import com.example.loannt.spendingnote.databinding.ActivityBlockBinding
import com.example.loannt.spendingnote.other.KEY_DATA
import com.example.loannt.spendingnote.other.getPreferences
import com.example.loannt.spendingnote.other.md5
import com.example.loannt.spendingnote.other.putPreferences

class BlockActivity : AppCompatActivity() {
    private lateinit var binding: ActivityBlockBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBlockBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val isSettingPass = intent.getBooleanExtra(KEY_DATA, false)
        if (isSettingPass) {
            binding.title.text = getString(R.string.settingPass)
            binding.btnClose.visibility = View.VISIBLE
        } else {
            binding.btnClose.visibility = View.INVISIBLE
        }
        binding.lockCustomView.password.observe(this) {
            if (!it.isNullOrEmpty() && it.length > 3) {
                if (!isSettingPass) {
                    //is check pass
                    if (checkMyPass(it)) {
                        Toast.makeText(this, getString(R.string.login_success), Toast.LENGTH_SHORT)
                            .show()
                        finish()
                    } else {
                        binding.message.setTextColor(Color.RED)
                        binding.message.text = getString(R.string.pass_incorrect)
                    }
                } else {
                    //is setting pass
                    putPreferences(KEY_PASSWORD, it.md5())
                    Toast.makeText(
                        this,
                        getString(R.string.setting_pass_success),
                        Toast.LENGTH_SHORT
                    ).show()
                    finish()
                }
            } else {
                binding.message.setTextColor(Color.RED)
                binding.message.text = getString(R.string.warning_pass)
            }
        }
        binding.btnClose.setOnClickListener {
            finish()
        }
    }

    private fun checkMyPass(pass: String?): Boolean {
        if (pass?.md5() == getPreferences(KEY_PASSWORD)) {
            return true
        }
        return false
    }

    override fun onBackPressed() {

    }
}