package com.example.loannt.spendingnote.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.loannt.spendingnote.data.model.CategoryNote
import com.example.loannt.spendingnote.data.model.NoteEntity

@Database(entities = [NoteEntity::class, CategoryNote::class], exportSchema = false, version = 1)
abstract class NoteDatabase : RoomDatabase() {
    abstract fun dao(): NoteDao

    companion object {
        @Volatile
        private var instance: NoteDatabase? = null

        fun getInstance(
            context: Context
        ): NoteDatabase = instance ?: synchronized(this) {
            buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context): NoteDatabase {
            return Room.databaseBuilder(
                context,
                NoteDatabase::class.java,
                "note_database"
            )
                .fallbackToDestructiveMigration()
                .build()
        }
    }

}