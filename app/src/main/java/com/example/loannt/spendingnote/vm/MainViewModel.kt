package com.example.loannt.spendingnote.vm

import android.app.Application
import android.content.Context.MODE_PRIVATE
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.loannt.spendingnote.data.db.NoteRepository
import com.example.loannt.spendingnote.data.db.NoteDatabase
import com.example.loannt.spendingnote.data.model.CategoryNote
import com.example.loannt.spendingnote.data.model.CategoryWithNotes
import com.example.loannt.spendingnote.data.model.NoteEntity
import com.example.loannt.spendingnote.other.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*

/**
 * ViewModel to access database share by activity
 */
class MainViewModel(app: Application) : ViewModel() {
    init {
        checkInitData(app)
    }

    /**
     * home fragment
     */
    private val repoData = NoteRepository(NoteDatabase.getInstance(app).dao())
    val allCategory = repoData.getAllCategory()

    val allCategoryExpense = repoData.getCategoryWithNotes(EnumTypeNote.Expense.name)
    val allCategoryIncome = repoData.getCategoryWithNotes(EnumTypeNote.Income.name)

    var typeNote = EnumTypeNote.Expense
    var typeHomeFilter: EnumArraySpinner = EnumArraySpinner.Month

    val dateStartFilter: Calendar by lazy {
        Calendar.getInstance()
    }
    val dateEndFilter: Calendar by lazy {
        Calendar.getInstance()
    }

    fun allTest(time: Long, end: Long) = repoData.findCategoryWithNotesByTime(
        "Expense",
        time,
        end
    )

    fun callDataStartToEnd(stTime: Long, eTime: Long) {
        dateStartFilter.timeInMillis = stTime
        dateEndFilter.timeInMillis = eTime
    }

    fun callDataByMonth(time: Long) {
        dateStartFilter.timeInMillis = time
        val end = Calendar.getInstance().apply {
            timeInMillis = time
            set(Calendar.DAY_OF_MONTH, 1)
            add(Calendar.MONTH, 1)
        }
        dateEndFilter.timeInMillis = end.timeInMillis
    }

    fun callDataByYear(time: Long) {
        dateStartFilter.timeInMillis = time

    }

    fun findNotesDate(
        timeStart: Calendar,
        timeEnd: Calendar
    ): LiveData<MutableList<CategoryWithNotes>> {
        return repoData.getCategoryWithNotesBetweenTime(
            timeStart.timeInMillis,
            timeEnd.timeInMillis
        )
    }

    fun findNotesYear(time: Calendar): LiveData<MutableList<CategoryWithNotes>> {
        val timeSearch = time.clone() as Calendar
        val start = timeSearch.apply {
            set(Calendar.DAY_OF_YEAR, 1)
            set(Calendar.HOUR_OF_DAY, 0)
            set(Calendar.MINUTE, 0)
            set(Calendar.SECOND, 0)
        }
        val end = (start.clone() as Calendar).apply {
            add(Calendar.YEAR, 1)
        }
        return repoData.getCategoryWithNotesBetweenTime(start.timeInMillis, end.timeInMillis)
    }

    fun findNotesMonthHome(time: Calendar): LiveData<MutableList<CategoryWithNotes>> {
        val timeSearch = time.clone() as Calendar
        val start = timeSearch.apply {
            set(Calendar.DAY_OF_MONTH, 1)
            set(Calendar.HOUR_OF_DAY, 0)
            set(Calendar.MINUTE, 0)
            set(Calendar.SECOND, 0)
        }
        val end = (start.clone() as Calendar).apply {
            add(Calendar.MONTH, 1)
        }
        return repoData.getCategoryWithNotesBetweenTime(start.timeInMillis, end.timeInMillis)
    }

    /**
     * history fragment
     *
     */

    /** date is saved in viewModel, use in history fragment, to find data by date */
    val date: Calendar by lazy {
        Calendar.getInstance()
    }

    /** find all note in one month */
    fun findNotesMonth(time: Calendar): LiveData<MutableList<NoteEntity>> {
        val timeSearch = time.clone() as Calendar
        val start = timeSearch.apply {
            set(Calendar.DAY_OF_MONTH, 1)
            set(Calendar.HOUR_OF_DAY, 0)
            set(Calendar.MINUTE, 0)
            set(Calendar.SECOND, 0)
        }
        val end = (start.clone() as Calendar).apply {
            add(Calendar.MONTH, 1)
        }
        return repoData.getNotesBetweenTime(start.timeInMillis, end.timeInMillis)
    }

    fun insertNote(note: NoteEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            repoData.insert(note)
        }
    }

    fun insertCategory(categoryNote: CategoryNote) {
        viewModelScope.launch(Dispatchers.IO) {
            repoData.insertCategory(categoryNote)
        }
    }

    fun updateCategory(categoryNote: CategoryNote) {
        viewModelScope.launch(Dispatchers.IO) {
            repoData.updateCategory(categoryNote)
        }
    }

    fun deleteNote(note: NoteEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            repoData.delete(note)
        }
    }

    fun deleteCategory(id: Long) {
        viewModelScope.launch(Dispatchers.IO) {
            repoData.deleteCategory(id)
        }
    }

    fun update(note: NoteEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            repoData.update(note)
        }
    }

    /** init category data, this fun just be called in first time open app after install */
    private fun checkInitData(app: Application) {
        val pref = app.getSharedPreferences(KEY_PREF, MODE_PRIVATE)
        val dataInit = pref.getBoolean(KEY_PREF, false)
        if (!dataInit) {
            viewModelScope.launch(Dispatchers.IO) {
                var id = 0L
                EnumCategoryNote.values().forEach {
                    repoData.insertCategory(
                        CategoryNote(
                            it.nameCategory,
                            it.color,
                            it.ic,
                            EnumTypeNote.Expense.name
                        ).apply { categoryId = id }
                    )
                    id++
                }
                repoData.insertCategory(
                    CategoryNote(
                        "Salary",
                        EnumColor.Color9.name,
                        EnumIcon.Salary.name,
                        EnumTypeNote.Income.name
                    )
                )
            }
            pref.edit().putBoolean(KEY_PREF, true).apply()
        }
    }

}