package com.example.loannt.spendingnote.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.loannt.spendingnote.data.model.CategoryNote
import com.example.loannt.spendingnote.data.model.NoteEntity
import com.example.loannt.spendingnote.databinding.AdapterNoteDayBinding
import com.example.loannt.spendingnote.other.dateToString

class NoteDayAdapter(
    private val onClickCallBack: (note: NoteEntity) -> Unit,
    private val list: MutableList<NoteEntity> = mutableListOf(),
    private val listCategory: MutableMap<Long, CategoryNote> = mutableMapOf(),
    private val leftCallback: (e: NoteEntity) -> Unit,
    private val rightCallback: (e: NoteEntity) -> Unit
) :
    RecyclerView.Adapter<NoteDayAdapter.ViewHolder>() {
    private var context: Context? = null
    private val listMap = mutableMapOf<String, MutableList<NoteEntity>>() //list to show

    init {
        filterList()
    }

    inner class ViewHolder(private val binding: AdapterNoteDayBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SimpleDateFormat", "SetTextI18n")
        fun onBind(key: String, listNoteDay: MutableList<NoteEntity>) {
            context?.let { ct ->
                binding.tvTitle.text = key
                binding.rcvListNote.layoutManager =
                    LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                val noteAdapter = NoteAdapter(
                    list = listNoteDay,
                    listCategory = listCategory,
                    itemCallBack = onClickCallBack
                )
                binding.rcvListNote.adapter = noteAdapter
                noteAdapter.swipeCallback(leftCallback, rightCallback, ct).let {
                    ItemTouchHelper(it).attachToRecyclerView(binding.rcvListNote)
                }
                binding.itemLayout.setOnClickListener {
                    if (binding.rcvListNote.visibility == View.VISIBLE) {
                        binding.rcvListNote.visibility = View.GONE
                        binding.btnDropList.rotation = 180f
                    } else {
                        binding.rcvListNote.visibility = View.VISIBLE
                        binding.btnDropList.rotation = 0f
                    }
                }
            }
            listNoteDay.map {
                it.value
            }.sum().let {
                if (it < 0) {
                    binding.tvSum.text = it.toString()
                    binding.tvSum.setTextColor(Color.RED)
                } else {
                    binding.tvSum.text = "+$it"
                    binding.tvSum.setTextColor(Color.WHITE)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            AdapterNoteDayBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        context = parent.context
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        listMap.toList()[position].let {
            holder.onBind(it.first, it.second)
        }
    }

    override fun getItemCount(): Int = listMap.size

    private fun filterList() {
        listMap.clear()
        for (note in list) {
            note.date.dateToString()?.let {
                val listNoteDate = listMap[it] ?: mutableListOf()
                listNoteDate.add(note)
                listMap[it] = listNoteDate
            }
        }
        notifyDataSetChanged()
    }

    fun categoryChange(newCate: MutableMap<Long, CategoryNote>) {
        listCategory.clear()
        listCategory.putAll(newCate)
        notifyDataSetChanged()
    }

    fun listChange(newList: MutableList<NoteEntity>) {
        list.clear()
        list.addAll(newList)
        filterList()
    }

    fun search(query: String) {
        val listFilter = mutableListOf<NoteEntity>()
        listFilter.addAll(list.filter {
            it.note.contains(query) || it.detail.contains(query)
        })
        listMap.clear()
        for (note in listFilter) {
            note.date.dateToString()?.let {
                val listNoteDate = listMap[it] ?: mutableListOf()
                listNoteDate.add(note)
                listMap[it] = listNoteDate
            }
        }
        notifyDataSetChanged()
    }
}